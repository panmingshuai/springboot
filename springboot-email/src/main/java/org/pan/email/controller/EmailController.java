package org.pan.email.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  下午4:12:39
 *
 */
@RestController
public class EmailController {
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Value("${spring.mail.username}")
	private String name;
	
	@RequestMapping("/send")
	public void send(){
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(name);//自己的地址
		message.setTo("751402497@qq.com");//发送给其他人的邮箱地址
		message.setSubject("一封来自未来的邮件");
		message.setText("内容不知道的雷人");
		javaMailSender.send(message);
	}
}
