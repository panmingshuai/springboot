package org.pan.test.first;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.pan.firstBoot.first.controller.FirstController;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;



@SpringBootTest
public class FirstBootTest {

	private MockMvc mockMvc;
	
	@Before
	public void contextLoads() {
		mockMvc = MockMvcBuilders.standaloneSetup(new FirstController()).build();
	}
	
	@Test
	public void getHello() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/hello1").accept(MediaType.APPLICATION_JSON)).
		andExpect(MockMvcResultMatchers.status().isOk()).
		andExpect(MockMvcResultMatchers.content().string(Matchers.equalTo("hello boot world")));
	}
	
	@Test
	public void gethello2() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/hello2").accept(MediaType.APPLICATION_JSON)).
		andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(Matchers.equalTo("hello sb")));
	}
}
