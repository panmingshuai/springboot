package org.pan.firstBoot.first.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/first")
public class FirstController {
	
	@RequestMapping("/hello1")
	@ResponseBody
	String home(){
		return "hello boot world";
	}
	
	@RequestMapping("/hello2")
	@ResponseBody
	String school(){
		return "hello sb";
	}
}
