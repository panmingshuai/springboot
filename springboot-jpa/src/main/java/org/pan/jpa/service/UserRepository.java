package org.pan.jpa.service;

import java.util.List;

import org.pan.jpa.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * @author panmingshuai
 * @description 
 * @Time 2017年11月2日  下午5:54:30
 *
 */
public interface UserRepository extends JpaRepository<User, Long>{
	
	/**
	 * 按照jpa原则进行查询
	 * @param name
	 * @return
	 */
	List<User> findByNameLike(String name);
	
}
