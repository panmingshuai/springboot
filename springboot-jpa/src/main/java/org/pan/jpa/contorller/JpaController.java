package org.pan.jpa.contorller;

import org.pan.jpa.domain.User;
import org.pan.jpa.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月3日  下午4:43:58
 *
 */
@RestController
@RequestMapping("/jpa")
public class JpaController {
	
	@Autowired
	UserRepository userRepository;
	
	
	@RequestMapping("/jpa1")
	@ResponseBody
	public void setData(){
		User user = new User("pan123", 17);
		System.out.println("jpa1");
		userRepository.save(user);
	}
	
	@RequestMapping("/jpa2")
	@ResponseBody
	public User getData(){
		return userRepository.findByNameLike("pan%").get(0);
	}
	
}
