package org.pan.test.jsp;


import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pan.jpa.Application;
import org.pan.jpa.domain.User;
import org.pan.jpa.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月2日  下午6:00:12
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class UserRepositoryTest {
	
	@Autowired
	UserRepository userRepository;
	
	@Test
	public void proxy() throws Exception {
		System.out.println(userRepository.getClass());
	}
	
	@Test
	public void save() throws Exception{
		for(int i=0; i<10; i++){
			User user = new User("pan" + i, 16 + i);
			userRepository.save(user);
		}
	}
	
	@Test
	public void all() throws Exception{
		save();
		Assertions.assertThat(userRepository.findAll()).hasSize(10);
	}
	
	@Test
	public void findName() throws Exception {
		save();
		Assertions.assertThat(userRepository.findByNameLike("pan%")).hasSize(10);
	}
	
	@After
	public void destroy() throws Exception {
		userRepository.deleteAll();
	}
}
