package org.pan.elasticsearch.repository;

import org.pan.elasticsearch.bean.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  下午2:23:42
 *
 */
public interface UserRepository extends ElasticsearchRepository<User, Long>{

}
