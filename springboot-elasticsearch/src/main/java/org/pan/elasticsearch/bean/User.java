package org.pan.elasticsearch.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月6日  上午11:43:05
 *
 */
@Document(indexName = "user", type = "user", shards = 1, replicas = 0, refreshInterval = "-1")
/**
 * indexName 索引名
 * type 类型
 * shards  分片数         拥有更多的碎片可以提升索引执行能力，并允许通过机器分发一个大型的索引；
 * replicas  备份        拥有更多的副本能够提升搜索执行能力以及集群能力。
 * refreshInterval   刷新时间        单位秒
 *
 */
public class User {
	@Id
	private Long id;
	private String name;
	private Integer age;
	
	public User() {
	}
	
	public User(Long id, String name, Integer age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
	
}
