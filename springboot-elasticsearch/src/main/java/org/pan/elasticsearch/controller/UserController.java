package org.pan.elasticsearch.controller;

import org.pan.elasticsearch.bean.User;
import org.pan.elasticsearch.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  下午2:30:20
 *
 */
@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/list")
	public Iterable<User> list(){
		userService.insert(new User(1l,"pa1n",19));
		userService.insert(new User(2l,"pan123",19));
		userService.insert(new User(3l,"pan456",13));
		userService.insert(new User(4l,"pan789",16));
		
		return userService.find();
	}
}
