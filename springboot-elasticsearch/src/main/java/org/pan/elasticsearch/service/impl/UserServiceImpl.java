package org.pan.elasticsearch.service.impl;

import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.pan.elasticsearch.bean.User;
import org.pan.elasticsearch.repository.UserRepository;
import org.pan.elasticsearch.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  下午2:27:18
 *
 */
@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public void insert(User user) {
		userRepository.save(user);
		
	}

	@Override
	public Iterable<User> find() {
		QueryStringQueryBuilder qsqb = new QueryStringQueryBuilder("pan123");
		return userRepository.search(qsqb);
	}

}
