package org.pan.elasticsearch.service;

import org.pan.elasticsearch.bean.User;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  下午2:25:46
 *
 */
public interface UserService {
	void insert(User user);
	
	Iterable<User> find();
}
