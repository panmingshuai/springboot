package org.pan.jdbc.controller;

import java.util.List;

import org.pan.jdbc.dao.UserDao;
import org.pan.jdbc.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  上午11:00:44
 *
 */
@RestController
public class UserController {
	@Autowired
	private UserDao userDao;
	
	@RequestMapping("/insert")
	@ResponseBody
	public int insert(){
		return userDao.insert();
	}
	
	@RequestMapping("/select")
	@ResponseBody
	public List<User> select(){
		return userDao.findUserByName("pan");
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public int delete(){
		return userDao.delete();
	}
}
