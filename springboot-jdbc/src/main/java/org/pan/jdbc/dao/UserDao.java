package org.pan.jdbc.dao;

import java.util.List;

import org.pan.jdbc.entity.User;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  上午10:49:47
 *
 */
public interface UserDao {
	int insert();
	List<User> findUserByName(String name);
	int delete();
}
