package org.pan.jdbc.dao.impl;

import java.util.List;

import org.pan.jdbc.dao.UserDao;
import org.pan.jdbc.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  上午10:21:20
 *
 */
@Repository
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insert() {
		return jdbcTemplate.update("insert t_user(name_f,age_f) values(?,?)", "pan456", 21);
	}

	@Override
	public List<User> findUserByName(String name) {
		return jdbcTemplate.query("select id_f id,name_f name,age_f age from t_user where name_f like concat(?, '%')", new Object[]{name}, BeanPropertyRowMapper.newInstance(User.class));
		
	}

	@Override
	public int delete() {
		return jdbcTemplate.update("delete from t_user where id_f=?", 3l);
		
	}
	
	
}
