package org.test.mybatis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.pan.mybatis.entity.User;
import org.pan.mybatis.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月6日  上午11:42:54
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {
	
	@Autowired
	private UserMapper userMapper;
	
	@Test
	public void insert(){
		userMapper.addUser(new User("pan123", 19));
	}
	
	@Test
	public void select(){
		System.out.println(userMapper.findUserLikeName("pan"));
	}
	
	@Test
	public void delete(){
		userMapper.delUser(2l);
	}
	
}
