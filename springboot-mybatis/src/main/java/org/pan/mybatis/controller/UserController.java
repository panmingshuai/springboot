package org.pan.mybatis.controller;

import java.util.List;

import org.pan.mybatis.entity.User;
import org.pan.mybatis.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  上午9:34:05
 *
 */
@RestController
public class UserController {
	
	@Autowired
	private UserMapper userMapper;
	
	@RequestMapping("/insert")
	public void insert(){
		userMapper.addUser(new User("pan123", 19));
	}
	
	@RequestMapping("/select")
	@ResponseBody
	public List<User> select(){
		return userMapper.findUserLikeName("pan");
	}
	
	@RequestMapping("/delete")
	public void delete(){
		userMapper.delUser(2l);
	}
}
