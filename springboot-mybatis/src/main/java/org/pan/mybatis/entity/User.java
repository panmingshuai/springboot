package org.pan.mybatis.entity;
/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月6日  上午11:43:05
 *
 */
public class User {
	private Long id;
	private String name;
	private Integer age;
	
	public User() {
	}
	
	public User(String name, Integer age) {
		this.name = name;
		this.age = age;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
	
}
