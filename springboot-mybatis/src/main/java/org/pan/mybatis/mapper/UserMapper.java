package org.pan.mybatis.mapper;
/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月6日  下午12:41:48
 *
 */

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.pan.mybatis.entity.User;

public interface UserMapper {
	
	@Insert("insert t_user(name_f,age_f) values(#{name},#{age})")
	void addUser(User user);
	
	@Select("select id_f id,name_f name,age_f age from t_user where name_f like concat(#{name}, '%')")
	List<User> findUserLikeName(String name);
	
	@Delete("delete from t_user where id_f=#{id}")
	void delUser(Long id);
}
