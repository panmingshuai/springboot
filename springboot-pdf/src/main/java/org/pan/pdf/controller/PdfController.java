package org.pan.pdf.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author panmingshuai
 * @description 
 * @Time 2017年11月7日  下午12:46:28
 *
 */
@RestController
public class PdfController {
	
	@RequestMapping("/download")
	public void download(HttpServletRequest request, HttpServletResponse response) throws Exception{
		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/pdf");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=user.pdf");
		
		Document document = new Document();
		PdfWriter.getInstance(document, response.getOutputStream());
		
		document.open();
		
		PdfPTable table = new PdfPTable(3);
		
		PdfPCell cell = new PdfPCell();
		cell.setPhrase(new Paragraph("3"));
		table.addCell(cell);
		document.add(table);
		
		cell = new PdfPCell();
		cell.setPhrase(new Paragraph("pan"));
		table.addCell(cell);
		document.add(table);
		
		cell = new PdfPCell();
		cell.setPhrase(new Paragraph("17"));
		table.addCell(cell);
		document.add(table);
		
		document.close();
	}
}
